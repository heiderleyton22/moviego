/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controller;

/**
 *
 * @author lol
 */
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author david
 */

//Conexión a la base de datos

public class ConnectionMSql {
    public String bd = "";
    public String url = "jdbc:mysql://localhost:3306/";
    public String user = "root";
    public String password = "";
    public String driver = "com.mysql.cj.jdbc.Driver";
    public Connection cx;
    
    public ConnectionMSql(String bd){
        this.bd = bd;
    }
    
    public Connection conectar(){
        try {
            Class.forName(driver);
            cx = (Connection) DriverManager.getConnection(url + bd,user,password);
            System.out.println("SE CONECTO A BD " + bd);
        } catch (ClassNotFoundException  | SQLException ex) {
            System.out.println("NO SE CONECTO A BD " + bd);
            Logger.getLogger(ConnectionMSql.class.getName()).log(Level.SEVERE, null, ex);
        }
        return cx;
    }
    
    public void desconectar (){
        try {
            cx.close();
            System.out.println("SE DESCONECTO " + bd);
        } catch (SQLException ex) {
            System.out.println("NO SE DESCONECTO " + bd);
            Logger.getLogger(ConnectionMSql.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
}

